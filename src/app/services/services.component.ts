import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Product } from './product';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  employees = [];
  products: Product[];

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeService.getEmployees()
      .subscribe(data => this.employees = data,
        error => {
          console.log('Error ', error);
        },
        function() {
          console.log('Got Data successfully!');
        });

      this.employeeService.getProduct()
      .subscribe(data => this.products = data,
        error => {
          console.log('Error ', error);
        },
        function() {
          console.log('Got Data successfully!');
        });
  }

}
