import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-directive',
  templateUrl: './directive.component.html',
  styleUrls: ['./directive.component.css']
})
export class DirectiveComponent implements OnInit {
  color = 'blue';
  shouldDisplayNGIF = true;
  @Input() parentData;
  name = 'Vineed Gangadharan';
  data = {
    key1: 'value1',
    key2: 'value2'
  };
  numVal = 5.567;

  @Output() childEvent  = new EventEmitter();

  names = ['vineed', 'dinesh', 'vijay', 'suraj'];

  constructor() { }

  ngOnInit() {
  }

  fireEvent() {
    console.log('Sending data to child...');
    this.childEvent.emit('Hey Parent!');
  }

}
