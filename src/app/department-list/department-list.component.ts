import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-department-list',
  template: `
    <ul class="items">
      <li *ngFor="let department of departments">
        <span class="badge">{{department.id}}</span>{{department.name}}
      </li>
    </ul>
  `,
  styles: [
    `
      .items{
        font-size:20px;
      }
      .items li{
        background-color:red;color:blue;
      }
    `
  ]
})
export class DepartmentListComponent implements OnInit {

  departments = [
    {id: 1, name: 'Angular'},
    {id: 2, name: 'Node'},
    {id: 3, name: 'MongoDB'},
    {id: 4, name: 'Ruby'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
