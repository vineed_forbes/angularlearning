import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Vineed Gangadharan';
  isDisabled = true;
  success = 'text-success';
  isSpecial = true;
  isMale = false;
  highlightedColor = 'cyan';
  name = 'I am app component';
  messageFromChild = 'None';

  multAttrStyle = {
    color: 'orange',
    fontStyle: 'italic'
  };

  messageClasses = {
    'text-success': this.isDisabled,
    'text-danger': this.isDisabled,
    'text-special': true
  };

  onButtonClick(event) {
    this.title = 'Change is every thing! ';
    console.log(event.event.target.id === 'b1');
  }
}
