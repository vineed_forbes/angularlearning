import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-list',
  template: `
    <div class='card'>
      <ul class="items">
        <li *ngFor="let department of departments">
          <span class="badge">{{department.id}}</span>{{department.name}}
        </li>
      </ul>
    </div>
    <button class="btn btn-primary">Hello bootstrap</button>
    <button class="btn btn-danger">Danger</button>
  `,
  styles: []
})
export class EmployeeListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
