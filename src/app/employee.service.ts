import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IEmployee } from './services/employee';
import { Observable } from 'rxjs';
import { Product } from './services/product';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  // private _url = '/assets/data/employees.json';
   private _url = 'http://www.mocky.io/v2/5c35bc663000007e0021b4c4';
  // private _url = 'http://www.mocky.io/v2/5c35f2fc3000008d0021b69a';

  private local_url = 'http://localhost:8080/MStrong/product';

  constructor(private httpClient: HttpClient) { }

  getEmployees(): Observable<IEmployee[]> {
    return this.httpClient.get<IEmployee[]>(this._url);
  }

  getProduct(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.local_url);
  }
}
